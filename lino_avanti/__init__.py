# -*- coding: UTF-8 -*-
# Copyright 2017-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This is the main module of Lino Avanti.

.. autosummary::
   :toctree:

   lib


"""

# from .setup_info import SETUP_INFO

__version__ = '25.1.2'

intersphinx_urls = dict(docs="https://avanti.lino-framework.org")
srcref_url = 'https://gitlab.com/lino-framework/avanti/blob/master/%s'
# doc_trees = ['docs']
