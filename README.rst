Lino Avanti is a `Lino <https://www.lino-framework.org/>`__ application used by
social workers in East Belgium for helping immigrants with their *integration
course*.

The **integration course** is a Belgian administrative procedure consisting in
an individual mentoring and a series of courses with the goal of helping new
residents to acquire the base knowledge about society and social relations in
Belgium in order to ease their integration on the territory.  Read more about
the procedure in `Parcours d’intégration des primo-arrivants
<http://socialsante.wallonie.be/?q=action-sociale/integration-personne-origine-etrangere/dispositifs/parcours-integration-primo-arrivant>`__
(French) or `Integrationsparcours: Der Einstiegsweg für Migranten
<http://www.dglive.be/desktopdefault.aspx/tabid-4795/8506_read-47195/>`__
(German).

Lino Avanti has been developed on behalf of the Ministery of the
`German-speaking Community of Belgium
<https://en.wikipedia.org/wiki/German-speaking_Community_of_Belgium>`__
(`Ministerium der Deutschsprachigen Gemeinschaft <https://www.dglive.be>`__).

- Project documentation: https://avanti.lino-framework.org
- Source code: https://gitlab.com/lino-framework/avanti
- Developer documentation: https://dev.lino-framework.org/specs/avanti
- End-user documentation: https://using.lino-framework.org/
- Changelog: https://avanti.lino-framework.org/changes
- Author: https://www.synodalsoft.net
