.. _avanti.changes:

======================
Changes in Lino Avanti
======================

Release notes:

.. toctree::
   :maxdepth: 1


   coming
   19.5.0
   18.10
   old

Change notes:

.. toctree::
   :maxdepth: 1

   2021
   2020
   2019
