.. _avanti:

===========
Lino Avanti
===========

Welcome to the **Lino Avanti** project homepage.

.. include:: ../README.rst



Content
========

.. toctree::
   :maxdepth: 1

   install/index
   guide/index
   changes/index
   eutests
   copyright
